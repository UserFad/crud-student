package org.devOps.Controller;

import org.devOps.Model.Student;
import org.devOps.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StudentController {

	@Autowired
	private StudentService ss;
	
	@RequestMapping("/students")
	public String viewHomePage(Model m) {
		m.addAttribute("students",ss.getStudents());
		return "index";
	}
	
	@RequestMapping("/newStudentForm")
	public String newStudentForm(Model m) {
		Student s = new Student();
		m.addAttribute("student",s);
		return "new_student";		
	}
	
	@RequestMapping("/saveStudent")
	public String saveStudent(@ModelAttribute("student") Student s) {
		ss.addStudent(s);
		return "redirect:/students";
	}
	
	@RequestMapping("/showUpdateForm/{id}")
	public String showUpdateForm(@PathVariable(value="id") long id,Model m) {
		Student s1=ss.getStudent(id);
		m.addAttribute("student",s1);
		return "update_student";
	}
	
	@RequestMapping("/deleteStudent/{id}")
	public String deleteStudent(@PathVariable(value="id") long id,Model m) {
		ss.deleteStudent(id);
		return "redirect:/students";
	}
}
