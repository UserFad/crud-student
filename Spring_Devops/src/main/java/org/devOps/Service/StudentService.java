package org.devOps.Service;

import java.util.List;

import org.devOps.Model.Student;

public interface StudentService {

	List<Student> getStudents();
	void addStudent(Student s);
	Student getStudent(long id);
	void deleteStudent(long id);
}
