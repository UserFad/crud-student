package org.devOps.Service;

import java.util.List;
import java.util.Optional;

import org.devOps.Model.Student;
import org.devOps.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService{

	@Autowired
	private StudentRepository sr;
	
	@Override
	public List<Student> getStudents() {
		return sr.findAll(); //this method return the list of all students existing in the database
		
	}

	@Override
	public void addStudent(Student s) {
		sr.save(s);
	}

	@Override
	public Student getStudent(long id) {
		Optional<Student> o=sr.findById(id);
		Student s1=null;
		if(o.isPresent()) {
			s1=o.get();
		}else {
			throw new RuntimeException("Student not found");
		}
		return s1;
		}

	@Override
	public void deleteStudent(long id) {
		sr.deleteById(id);
	}

}
